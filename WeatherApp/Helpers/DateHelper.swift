//
//  DateModel.swift
//  WeatherApp
//
//  Created by Rush on 10/06/2019.
//  Copyright © 2019 Rush. All rights reserved.
//

import Foundation

struct DateHelper
{
    let currentDateMillis = { () -> Int64 in
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        formatter.timeStyle = .medium
        formatter.timeZone = TimeZone.current
        print(formatter.timeZone as Any)
        let current = formatter.string(from: Date())
        print(current)
        let stringCurrent = formatter.date(from: current)
        print(stringCurrent as Any)
        let inMillis = stringCurrent!.timeIntervalSince1970
        print(inMillis)
        return Int64(inMillis)
    }
}

typealias UnixTime = Int64

extension UnixTime {
    private func formatType(form: String) -> DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.dateFormat = form
        return dateFormatter
    }
    var dateFull: Date {
        return Date(timeIntervalSince1970: Double(self))
    }
    var toHour: String {
        return formatType(form: "HH:mm").string(from: dateFull)
    }
    var toDay: String {
        return formatType(form: "MM/dd/yyyy").string(from: dateFull)
    }
}
