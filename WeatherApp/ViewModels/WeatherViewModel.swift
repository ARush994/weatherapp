//
//  WetherViewModel.swift
//  WeatherApp
//
//  Created by Rush on 05/06/2019.
//  Copyright © 2019 Rush. All rights reserved.
//

import Foundation
import UIKit
import SwiftGifOrigin

class WeatherViewModel
{
    // MARK: - Singleton organization
    private static let instance = WeatherViewModel()
    // Initialization
    private init()
    {
        
    }
    // MARK: - Accessors
    static func getInstance() -> WeatherViewModel {
        return instance
    }
    ///////////////////////////////////
    
    // links to view elements
    var view: UIView!
    var backgroundImageView = UIImageView()
    
    var temperSwitch: UISwitch!                 // link to a swith
    var temperSign: UILabel!                    // link to a label in one line with swith
    var segmentedControl: UISegmentedControl!   // stripe with changing of cities
    var showForecastButton: UIButton!           // Show Forecast button
    var forecastTable: UITableView!             // table with 3-hour forecast
    var todayTempLabel: UILabel!                // temperature in fixed area
    var todayDateTime: UILabel!                 // date and time in fixed area
    var todayHumLabel: UILabel!                 // humidity in fixed area
    var todayWeatherImage: UIImageView!         // image in fixed area
    //////////////////////////////////
    
    var temperStringOnSwitch = TemperatureTypes.Celsius.description
    var TemperStringOnSwitch:String             // viewModel variable for string temper label
    {
        get{
            return temperStringOnSwitch
        }
        set{
            temperStringOnSwitch = newValue
            temperSign?.text = newValue
            if newValue.elementsEqual(TemperatureTypes.Celsius.description)
            {
                temperSwitch.isOn = false
                temperSignOnSwitch = TemperatureTypes.Celsius.sign
            }
            else
            {
                temperSwitch.isOn = true
                temperSignOnSwitch = TemperatureTypes.Fahrenheit.sign
            }
        }
    }
    var temperSignOnSwitch = TemperatureTypes.Celsius.sign // sign of temperature(F,C)
    
    // variables for fetching info
    var selectedCity = CityNameTypes.Donetsk
    var SelectedCity:  CityNameTypes    // selected city with segmentControl
    {
        get {
            return selectedCity
        }
        set {
            self.selectedCity = newValue
            self.segmentedControl.selectedSegmentIndex = newValue.rawValue
            self.currentId = newValue.id
        }
    }
    
    // weather for current session
    var weatherFirstObject = WeatherDataModel() // first weather data
    var weatherObject = WeatherDataModel() // temporary weather data object
    var weatherDataForSixDays: [WeatherDataModel] = [] // weather data array
    
    var currentId = ""      // id of the selected city
    
    // before rendering the view
    func preparingView(view:UIView)
    {
        segmentedControl.layer.cornerRadius = 5
        showForecastButton.layer.cornerRadius = 5;
        
        temperSwitch.isOn = false;
        temperSign?.text = temperStringOnSwitch
        
        // adding imageView to bacskground
        view.addSubview(backgroundImageView)
        view.sendSubviewToBack(backgroundImageView)
        backgroundImageView.translatesAutoresizingMaskIntoConstraints = false
        backgroundImageView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        backgroundImageView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        backgroundImageView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        backgroundImageView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        backgroundImageView.alpha = 0.5
        setBackgroundImg(imgName: "AppBackground")
        
        forecastTable.backgroundView?.alpha = 0.1
    }
    
    func setBackgroundImg(imgName:String)
    {
        backgroundImageView.image = UIImage(named: imgName)
    }
    
    func setBackgroundGif(gifName:String)
    {
        backgroundImageView.image = UIImage.gif(name: gifName)
    }
    
    
    func temperFormatSwitching() {
        if temperSwitch.isOn
        {
            temperStringOnSwitch = TemperatureTypes.Fahrenheit.description
            temperSignOnSwitch = TemperatureTypes.Fahrenheit.sign
        }
        else
        {
            temperStringOnSwitch = TemperatureTypes.Celsius.description
            temperSignOnSwitch = TemperatureTypes.Celsius.sign
        }
        temperSign?.text = temperStringOnSwitch
    }
    
    // select city
    func selectCity(selectedSegmentIndex:Int)
    {
        switch selectedSegmentIndex
        {
        case 0:
            selectedCity = CityNameTypes.Donetsk
        case 1:
            selectedCity = CityNameTypes.Mosсow
        case 2:
            selectedCity = CityNameTypes.Krasnodar
        default:
            selectedCity = CityNameTypes.Donetsk
        }
    }
    
    // change city id
    func changingCity()
    {
        currentId = selectedCity.id
    }
    
    // update wether data for now, changing background
    func updateUIFixedBlock()
    {
        todayTempLabel.text = "\(weatherObject.temperature)\(temperSignOnSwitch)"
        todayDateTime.text = "\(weatherObject.date)"
        todayHumLabel.text = "\(weatherObject.humidity)%"
        todayWeatherImage.image = UIImage(named: weatherObject.weatherIconName)
        switch weatherObject.weatherIconName {
        case "sunny":
            setBackgroundGif(gifName: "SunnyWallpaperGif")
        case "swow4", "snow5":
            setBackgroundGif(gifName: "SnowWallpaperGif")
        case "shower3", "light_rain":
            setBackgroundGif(gifName: "ShowerWallpaperGif")
        case "cloudy2", "fog":
            setBackgroundGif(gifName: "CloudyWallpaperGif")
        case "tstorm1":
            setBackgroundImg(imgName: "TStormWallpaper")
        case "tstorm3":
            setBackgroundGif(gifName: "TStormWallpaper3Gif")
        default:
            setBackgroundImg(imgName: "AppBackground")
        }
    }
    
    //This method turns a condition code into the name of the weather condition image
    func updateWeatherIcon(condition: Int) -> String
    {
        switch (condition) {
        case 0...300 :
            return "tstorm1"
        case 301...500 :
            return "light_rain"
        case 501...600 :
            return "shower3"
        case 601...700 :
            return "snow4"
        case 701...771 :
            return "fog"
        case 772...799 :
            return "tstorm3"
        case 800 :
            return "sunny"
        case 801...804 :
            return "cloudy2"
        case 900...903, 905...1000  :
            return "tstorm3"
        case 903 :
            return "snow5"
        case 904 :
            return "sunny"
        default :
            return "dunno"
        }
    }
    
    // MARK: - NetworkingModule
    func downloadData()
    {
        ModulesManager.getNetworkingInstance().getWeatherData(cityId: currentId)
    }
    
    // MARK: - CoreDataModule
    func loadData()
    {
        ModulesManager.getCoreDataInstance().loadData()
    }
    func saveData()
    {
        ModulesManager.getCoreDataInstance().saveData()
    }
}
