//
//  ModulesManager.swift
//  WeatherApp
//
//  Created by Rush on 16/06/2019.
//  Copyright © 2019 Rush. All rights reserved.
//

import Foundation

class ModulesManager{
    // MARK: - Accessor to CoreDataModule - singleton
    static func getCoreDataInstance() -> CoreDataModule {
        return CoreDataModule.getInstance()
    }
    
    // MARK: - Accessor to NetworkingModule - singleton
    static func getNetworkingInstance() -> NetworkingModule {
        return NetworkingModule.getInstance()
    }
}
