//
//  CoreDataWork.swift
//  WeatherApp
//
//  Created by Rush on 10/06/2019.
//  Copyright © 2019 Rush. All rights reserved.
//

import UIKit
import CoreData

class CoreDataModule
{
    // MARK: - Singleton organization
    private static let instance = CoreDataModule()
    // Initialization
    private init()
    {
        nsWeatherData = []
        nsSupportInfoData = []
    }
    // MARK: - Accessors
    static func getInstance() -> CoreDataModule {
        return instance
    }
    ///////////////////////////////////
    
    let viewModel = WeatherViewModel.getInstance()
    
    // saving weather
    var nsWeatherData: [NSManagedObject]       // weather data with the type of CoreData
    var nsSupportInfoData: [NSManagedObject]   // support info data with the type of CoreData
    
    // weather and date for current session
    var dateModel: DateHelper = DateHelper()          // date object
    var weatherObject: WeatherDataModel = WeatherDataModel() // item of weather data
    
    var container: NSPersistentContainer!
    
    // MARK: - Savinging data to CoreData
    func saveData()
    {
        deleteData()
        
        // adding to database all days weather for city
        nsWeatherData = []
        nsSupportInfoData = []
        
        saveWeaterItem(viewModel.weatherFirstObject)
        for weatherForMoment in viewModel.weatherDataForSixDays
        {
            saveWeaterItem(weatherForMoment)
        }
        
        let weatherDataForCity = WeatherDataForCityModel(switchPosition: viewModel.temperSwitch.isOn, city: viewModel.selectedCity.rawValue, dateMillis: dateModel.currentDateMillis())
        saveSupportInfoItem(weatherDataForCity)
        // encoding custum object
        //let encoded = NSKeyedArchiver.archivedData(withRootObject: people)
        //UserDefaults.standard.set(encoded, forKey: "encodedData")
    }
    
    func saveWeaterItem(_ weatherItem : WeatherDataModel)
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let context = appDelegate.persistentContainer.viewContext
        let database = NSEntityDescription.entity(forEntityName: "WeatherItem", in: context)!
        
        let nsWeatherItem = NSManagedObject(entity: database, insertInto: context)
        nsWeatherItem.setValue(weatherItem.date, forKeyPath: "date")
        nsWeatherItem.setValue(weatherItem.temperature, forKeyPath: "temperature")
        nsWeatherItem.setValue(weatherItem.humidity, forKey: "humidity")
        nsWeatherItem.setValue(weatherItem.condition, forKey: "condition")
        nsWeatherItem.setValue(weatherItem.weatherIconName, forKey: "weatherIconName")
        
        do {
            try context.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func saveSupportInfoItem(_ supportInfoItem : WeatherDataForCityModel)
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let database = NSEntityDescription.entity(forEntityName: "SupportInfoItem", in: context)!
        let nsSupportInfoItem = NSManagedObject(entity: database, insertInto: context)
        nsSupportInfoItem.setValue(supportInfoItem.switchPosition, forKeyPath: "switchPosition")
        nsSupportInfoItem.setValue(supportInfoItem.city, forKeyPath: "city")
        nsSupportInfoItem.setValue(supportInfoItem.dateMillis, forKeyPath: "dateMillis")
        
        do {
            try context.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    // MARK: - Deleting all data from CoreData
    func deleteData()
    {
        deleteEntityData("SupportInfoItem")
        deleteEntityData("WeatherItem")
    }
    
    func deleteEntityData(_ entity: String){
        
        //As we know that container is set up in the AppDelegates so we need to refer that container.
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        //We need to create a context from this container
        let context = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        
        do {
            try context.execute(deleteRequest)
            do{
                try context.save()
            }
            catch
            {
                print(error)
            }
        } catch let error {
            print("Detele all data in \(entity) error :", error)
        }
    }
    
    // MARK: - Loading data from CoreData (if it present)
    func loadData()
    {
        var dateMillis : Int64
        var dateMillisNow : Int64
        viewModel.SelectedCity = CityNameTypes.Donetsk
        nsSupportInfoData = loadWeatherData(entityNameString: "SupportInfoItem")
        if nsSupportInfoData.count != 0
        {
            // restoring support data from CoreData
            for item in nsSupportInfoData // repeatting one time
            {
                let isOn = item.value(forKeyPath: "switchPosition") as! Bool
                if isOn{ viewModel.TemperStringOnSwitch = TemperatureTypes.Fahrenheit.description }
                else { viewModel.TemperStringOnSwitch = TemperatureTypes.Celsius.description}
                
                let cityNum = item.value(forKeyPath: "city") as! Int
                viewModel.SelectedCity = CityNameTypes(rawValue: cityNum)!
                // working with date
                dateMillis = item.value(forKeyPath: "dateMillis") as! Int64
                dateMillisNow = dateModel.currentDateMillis()
                if dateMillis.toDay.elementsEqual(dateMillisNow.toDay)
                {
                    var firstElement = false
                    nsWeatherData = loadWeatherData(entityNameString: "WeatherItem")
                    for item in nsWeatherData   // restoring wether data fron CoreData
                    {
                        weatherObject = WeatherDataModel()
                        weatherObject.date = item.value(forKeyPath: "date") as! String
                        weatherObject.temperature = item.value(forKey: "temperature") as! Int
                        weatherObject.humidity = item.value(forKey: "humidity") as! Int
                        weatherObject.condition = item.value(forKey: "condition") as! Int
                        weatherObject.weatherIconName = item.value(forKey: "weatherIconName") as! String
                        if !firstElement    // setting data to fixed area
                        {
                            viewModel.weatherFirstObject = weatherObject
                            viewModel.weatherObject = weatherObject
                            viewModel.updateUIFixedBlock()
                            firstElement = true
                        }
                        else    // appending array rendering on table
                        {
                            viewModel.weatherDataForSixDays.append(weatherObject)
                        }
                    }
                }
                else
                {
                    // getting same data from network with json (if date is not today's date)
                    viewModel.downloadData()
                }
            }
        }
        else
        {
            // getting same data from network with json (if there is empty CoreData)
            viewModel.downloadData()
        }
    }
    
    func loadWeatherData(entityNameString: String) -> [NSManagedObject]
    {
        var nsData: [NSManagedObject] = []
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: entityNameString)
        
        do {
            nsData = try context.fetch(fetchRequest)
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        return nsData
    }
}
