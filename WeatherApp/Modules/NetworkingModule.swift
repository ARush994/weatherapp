//
//  NetWork.swift
//  WeatherApp
//
//  Created by Rush on 10/06/2019.
//  Copyright © 2019 Rush. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class NetworkingModule
{
    // MARK: - Singleton organization
    private static let instance = NetworkingModule()
    // Initialization
    private init()
    { }
    // MARK: - Accessors
    static func getInstance() -> NetworkingModule {
        return instance
    }
    ///////////////////////////////////
    
    // in this class all data storing
    let viewModel = WeatherViewModel.getInstance()
    // temporary object for tranferring weatger data
    var weatherObject = WeatherDataModel() // item of weather data
    // request URL
    let WEATHER_URL = "http://api.openweathermap.org/data/2.5/forecast"
    // parameters for Alamofire request
    var params = { (id: String) -> [String : String] in
        let APP_ID = "8e716e3c6ed9ed91d5436580d3212fe5"
        return ["id" : id,
            "appid" : APP_ID]
    }
    
    // MARK: - Networking
    func getWeatherData(cityId: String) {
        Alamofire.request(WEATHER_URL, method: .get, parameters: params(cityId)).responseJSON { (responseObject) in
            if let json = responseObject.result.value
            {
                print("JSON: \(String(describing: json))")
                self.updateWeatherData(json: JSON(json as Any))
            }
            else
            {
                print("FAIL REQUEST")
            }
        }
    }
    
    // MARK: - JSON parsing
    func updateWeatherData(json : JSON) {
        viewModel.weatherDataForSixDays = []
        var firstElement = false
        for item in json["list"].arrayValue
        {
            weatherObject = WeatherDataModel()
            if var temp = item["main"]["temp"].double
            {
                if viewModel.temperSignOnSwitch.elementsEqual(TemperatureTypes.Celsius.sign)
                {
                    temp -= 273.15;
                }
                weatherObject.date = item["dt_txt"].stringValue
                weatherObject.temperature = Int(temp);
                weatherObject.humidity = item["main"]["humidity"].intValue
                weatherObject.condition = item["weather"][0]["id"].intValue
                weatherObject.weatherIconName = viewModel.updateWeatherIcon(condition: weatherObject.condition)
                if !firstElement
                {
                    viewModel.weatherFirstObject = weatherObject
                    viewModel.weatherObject = weatherObject
                    viewModel.updateUIFixedBlock()
                    firstElement = true
                }
                else
                {
                    viewModel.weatherDataForSixDays.append(weatherObject)
                }
            }
        }
        viewModel.forecastTable.reloadData()
    }
}
