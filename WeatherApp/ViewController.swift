//
//  ViewController.swift
//  WeatherApp
//
//  Created by Rush on 30/05/2019.
//  Copyright © 2019 Rush. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    let backgroundImageView = UIImageView() // background image
    var viewModel = WeatherViewModel.getInstance()     // all application data
    var weatherObject: WeatherDataModel = WeatherDataModel() // temporary weather data object
    
    // MARK: - IBOutlet UI elements
    @IBOutlet weak var temperSwitch: UISwitch!
    @IBOutlet weak var temperSign: UILabel!
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    @IBOutlet weak var showForecastButton: UIButton!
    @IBOutlet weak var todayDateTime: UILabel!
    @IBOutlet weak var todayTempLabel: UILabel!
    @IBOutlet weak var todayHumLabel: UILabel!
    @IBOutlet weak var todayWeatherImage: UIImageView!
    
    @IBOutlet weak var forecastTable: UITableView!
    //////////////////////////////////////////////
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.temperSwitch = temperSwitch
        viewModel.temperSign = temperSign
        viewModel.segmentedControl = segmentedControl
        viewModel.showForecastButton = showForecastButton
        viewModel.forecastTable = forecastTable
        viewModel.todayTempLabel = todayTempLabel
        viewModel.todayDateTime = todayDateTime
        viewModel.todayHumLabel = todayHumLabel
        viewModel.todayWeatherImage = todayWeatherImage
    }
    
    override func viewWillAppear(_ animated: Bool) {
        viewModel.preparingView(view: view)
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(appMovedToBackground), name: UIApplication.willResignActiveNotification, object: nil)
        
        forecastTable.dataSource = self;
        viewModel.loadData()
    }
    
    @IBAction func temperFormatSwitching(_ sender: Any) {
        viewModel.temperFormatSwitching()
    }
    
    @IBAction func changeCity(_ sender: Any) {
        viewModel.selectCity(selectedSegmentIndex: segmentedControl.selectedSegmentIndex)
    }
    
    @IBAction func showingForecast(_ sender: Any) {
        print("Forecast is fetching for \(viewModel.selectedCity)")
        
        let alertController = UIAlertController(title: "Information", message:
            "Forecast is fetching for \(viewModel.selectedCity) city", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Understood", style: .default))
        self.present(alertController, animated: true, completion: nil)
        
        viewModel.changingCity()//showForacastForCity()
        viewModel.loadData()
    }
    
    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        if UIApplication.shared.statusBarOrientation.isLandscape{
            print("landscape orientation")
        }
        else
        {
            print("vertical orientation")
        }
    }
    
    @objc func appMovedToBackground() {
        // applicationWillResignActive - second method,
        // that can make thms when exitting
        print("App moved to background!")
        viewModel.saveData()
    }
}

// MARK: - UITableviewDataSource
extension ViewController: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.weatherDataForSixDays.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
        weatherObject = WeatherDataModel()
        weatherObject = viewModel.weatherDataForSixDays[indexPath.row];
        cell?.textLabel?.text = "\(weatherObject.temperature)\(viewModel.temperSignOnSwitch), Hum.: \(weatherObject.humidity)%"
        cell?.detailTextLabel?.text = "Date: \(weatherObject.date)"
        cell?.imageView?.image = UIImage(named: weatherObject.weatherIconName)
        return cell!
    }
}



