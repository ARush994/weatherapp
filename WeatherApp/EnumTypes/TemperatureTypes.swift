//
//  TemperatureTypes.swift
//  WeatherApp
//
//  Created by Rush on 07/06/2019.
//  Copyright © 2019 Rush. All rights reserved.
//

import Foundation

enum TemperatureTypes: Int {
    case Celsius = 0
    case Fahrenheit = 1
    
    var description: String {
        switch self {
        case .Celsius:
            return "Celsius"
        case .Fahrenheit:
            return "Fahrenheit"
        }
    }
    
    var sign: String {
        switch self {
        case .Celsius:
            return "C"
        case .Fahrenheit:
            return "F"
        }
    }
}
