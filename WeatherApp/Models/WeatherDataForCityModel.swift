//
//  WetherDataForCityModel.swift
//  WeatherApp
//
//  Created by Rush on 05/06/2019.
//  Copyright © 2019 Rush. All rights reserved.
//

import Foundation

class WeatherDataForCityModel {
    var switchPosition : Bool
    var city : Int
    var dateMillis : Int64
    
    init()
    {
        switchPosition = false
        city = 0
        dateMillis = 0
    }
    
    init(switchPosition:Bool,
         city:Int,
         dateMillis:Int64)
    {
        self.switchPosition = switchPosition;
        self.city = city;
        self.dateMillis = dateMillis;
    }
}
