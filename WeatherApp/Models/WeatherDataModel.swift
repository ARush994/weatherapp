//
//  WeatherDataModel.swift
//  WeatherApp
//
//  Created by Rush on 01/06/2019.
//  Copyright © 2019 Rush. All rights reserved.
//

import Foundation
import ObjectMapper

class WeatherDataModel: Mappable {
    var date = ""
    var temperature = 0
    var humidity = 0
    var condition = 0
    var weatherIconName = ""
    
    required init?(map: Map) {
        
    }
    
    init()
    { }
    
    func mapping(map: Map) {
        date <- map["dt_txt"]
        humidity <- map["main.humidity"]
        condition <- map["weather.0.id"]
    }
    
    init(date : String,
         temperature : Int,
         humidity : Int,
         condition : Int,
         weatherIconName : String)
    {
        self.date = date
        self.temperature = temperature
        self.humidity = humidity
        self.condition = condition
        self.weatherIconName = weatherIconName
    }
}
